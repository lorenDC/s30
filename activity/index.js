// S30 Activity Template: 
db.fruits.insertMany([
        {
            "name": "Banana",
            "supplier": "Farmer Fruits Inc.",
            "stocks": 30,
            "price": 20,
            "onSale": true
        },
        {
            "name": "Mango",
            "supplier": "Mango Magic Inc.",
            "stocks": 50,
            "price": 70,
            "onSale": true
        },
        {
            "name": "Dragon Fruit",
            "supplier": "Farmer Fruits Inc.",
            "stocks": 10,
            "price": 60,
            "onSale": true
        },
        {
            "name": "Grapes",
            "supplier": "Fruity Co.",
            "stocks": 30,
            "price": 100,
            "onSale": true
        },
        {
            "name": "Apple",
            "supplier": "Apple Valley",
            "stocks": 0,
            "price": 20,
            "onSale": false
        },
        {
            "name": "Papaya",
            "supplier": "Fruity Co.",
            "stocks": 15,
            "price": 60,
            "onSale": true
        }
]);

// 1.) Use 3 stages aggregation pipeline to count the total number of fruits on sale, exclude the _id field.

// Code here:
    db.fruits.aggregate([
        {
            $match: {"onSale": true}
        },
        {
            $group: {_id: "supplier", total: {$sum: 1}}
        },
        {
             $project: { _id: 0 }
        }
    ]);


// 2.) Use 3 stages aggregation pipeline to count the total number of fruits with stock more than 20, and exclude the _id field.

// Code here:
    db.fruits.aggregate([
        {
            $match: {"stocks": {$gt: 20}}
        },
        {
            $group: {_id: "supplier", total: {$sum: 1}}
        },
        {
             $project: { _id: 0 }
        }
    ]);

// 3.) Use the average operator to get the average price of fruits onSale per supplier.

// Code here:

    db.fruits.aggregate([
        {
            $match: {"onSale": true}
        },
        {
            $group: {_id: "$supplier", avgAmount: {$avg: "$price"}}
        }
    ]);


// 4.) Use the max operator to get the highest price of a fruit per supplier.

// Code here:
    
     db.fruits.aggregate([
        {
            $group: {_id: "$supplier", maxAmount: {$max: "$price"}}
        }
    ]);


// 5. Use the min operator to get the lowest price of a fruit per supplier.

// Code here:

     db.fruits.aggregate([
        {
            $group: {_id: "$supplier", minAmount: {$min: "$price"}}
        }
    ]);